/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es: Daiana Inés Nuñez
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
/*==================[macros and definitions]=================================*/
uint32_t var= 0x01020304;
uint8_t var1, var2, var3, var4;
union Utest dato;
//typedef struct cada_byte cada_byte;
typedef struct cada_byte{
	uint8_t byte1;
	uint8_t byte2;
	uint8_t byte3;
	uint8_t byte4;
}cada_byte;
union Utest {
	uint32_t todos_los_bytes;
	cada_byte cada_byte;
};

/*==================[internal functions declaration]=========================*/

int main(void)
{
	printf("Dato 32 bits: %x\n", var);
	var1=(uint8_t) 0x00FF & var;
	var2=(uint8_t) 0x00FF & var>>8;
	var3=(uint8_t) 0x00FF & var>>16;
	var4=(uint8_t) 0x00FF & var>>24;
	printf("Primer byte: %d\n", var1);
	printf("Segundo byte: %d\n", var2);
	printf("Tercer byte: %d\n", var3);
	printf("Cuarto byte: %d\n\n", var4);
	//Parte b.
	printf("Parte B\n");
	dato.todos_los_bytes = 0x01020304;
	printf("Dato 32 bits: %x\n", dato.todos_los_bytes);
	printf("Primer byte: %d\n", dato.cada_byte.byte1);
	printf("Segundo byte: %d\n", dato.cada_byte.byte2);
	printf("Tercer byte: %d\n", dato.cada_byte.byte3);
	printf("Cuarto byte: %d\n", dato.cada_byte.byte4);
	return 0;

}

/*==================[end of file]============================================*/

