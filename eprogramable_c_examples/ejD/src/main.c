/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:Daiana Inés Nuñez
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <inttypes.h>
#include <unistd.h>

/*==================[macros and definitions]=================================*/
typedef struct
{
	uint8_t port;				/*!< GPIO port number */
	uint8_t pin;				/*!< GPIO pin number */
	uint8_t dir;				/*!< GPIO direction ‘0’ IN;  ‘1’ OUT */
} gpioConf_t;

/*==================[internal functions declaration]=========================*/

void cPuertos(uint8_t bcd, gpioConf_t *portPtr)
{
	uint8_t i;
	printf("El número ingresado es: %d \n",bcd);

	for(i=0;i<4;i++)
	{
		printf("El puerto %d.%d ",portPtr->port,portPtr->pin);
		if(bcd%2){
			printf(" está en alto. \r\n");			}
		else{
			printf(" está en bajo. \r\n");}
		bcd=bcd/2;
		portPtr++;
	}
}

int main(void)
{
	uint8_t bcd;
   	gpioConf_t port_arr[4], *portPtr;

   	port_arr[0].port=1;
   	port_arr[1].port=1;
   	port_arr[2].port=1;
   	port_arr[3].port=2;
   	port_arr[0].pin=4;
   	port_arr[1].pin=5;
   	port_arr[2].pin=6;
   	port_arr[3].pin=14;
   	portPtr=&port_arr;

   	printf("Ingrese un número de un solo dígito: \n");
   	scanf( "%" SCNu8, &bcd);
   	cPuertos(bcd,portPtr);

	return 0;
}
/*==================[end of file]============================================*/

