/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es: Daiana Inés Nuñez
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <inttypes.h>
#include <unistd.h>
#include "delay.h"
/*==================[macros and definitions]=================================*/
#define MAX_VALUE 999
#define WRITE_DELAY 1   // 1 ms
uint8_t digits_bcd [3]={0}; //Arreglo Global.
uint16_t display_value=0; //Variable Global.
gpio_t p_lines [4]; //Arreglos de estructuras Globales.
gpio_t sel_lines [3];
/*

 */


/*==================[internal functions declaration]=========================*/

bool verificar (uint16_t data) // Verifica si el valor ingresado es correcto.
{
	if (data<=MAX_VALUE){
		return true;
	}
	else{
		return false;
	}
}

void ToBcd (uint16_t data) //Pasa el número a BCD.
{
	uint8_t i;
	for (i=0;i<3;i++)
	{
		digits_bcd [2-i]= data%10; // Asigno el resto a la dirección donde apunta el puntero.
		data = data/10;
	}
}

void ToDisplay()
{
	uint8_t i, j, mask;
	for (i=0; i<3; i++)
	{
		mask = 1;
		for(j=0; j<4; j++)
		{
			if (digits_bcd[i] & mask)
			{
				GPIOOn(p_lines[j]);
			}
			else
			{
				GPIOOff(p_lines[j]);
			}
			mask = mask << 1;
		}
		GPIOOn(sel_lines[i]);
		DelayMs(WRITE_DELAY);
		GPIOOff(sel_lines[i]);
	}

}


bool ITSE0803Init(gpio_t * pins) // Inicia el Display.
{
	uint8_t i;
	// port D1, D2, D3,D4.
	for(i=0; i<4; i++)
	{
		GPIOInit(pins[i],GPIO_OUTPUT);
		p_lines[i]= pins[i];
	}
	for (i=4; i<7; i++)
		{
		GPIOInit(pins[i],GPIO_OUTPUT);
		sel_lines[i]=pins[i];
		}
	return (true);
}


bool ITSE0803DisplayValue(uint16_t data) // Asigna y muestra los valores.
{
	verificar(data);
	ToBcd(data);
	ToDisplay();
	display_value = data;
	return (true);
}

uint16_t ITSE0803ReadValue(void){

	return display_value;
}

bool ITSE0803Deinit(gpio_t * pins){

	GPIODeinit();
	return true;
}


int main(void)
{
    uint16_t data= 219;
    uint8_t *ptr_Bcd;
    uint8_t bcd = 5;
    gpioConf_t *portPtr;




    ToBcd(data, ptr_Bcd);
/*
    for (i=0;i<3;i++)
    		{
    		printf("Digito %" PRIu8 "\n", i);
    		printf("%" PRIu8 "\n", *ptr_Bcd);
    		ptr_Bcd++;
    	}
    */

    ToDisplay(ptr_Bcd, portPtr);


   /* bool aux;
    aux= verificar(data);
    if (aux) {
    	printf("El valor ingresado es correcto");
    }
    else{
    	printf("El valor ingresado es incorrecto");
    }*/

   /*Ejemplo teclas=Read();
    	if (Encender=true){
    	medir una distancia;
    	}
si no tengo que congelar, refresco el valor.
if oll =false, se debe actualizar el display;
    Delay();
    */

   	return 0;
}

/*==================[end of file]============================================*/

