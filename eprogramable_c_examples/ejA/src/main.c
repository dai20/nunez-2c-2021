/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:Daiana Inés Nuñez
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../ejA/inc/main.h"

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <inttypes.h>
#include <unistd.h>


/*==================[macros and definitions]=================================*/
typedef struct
{
	uint8_t n_led;       // indica el número de led a controlar.
	uint8_t n_ciclos;   //  indica la cantidad de ciclos de encendido/apagado.
	uint8_t periodo;    //  indica el tiempo de cada ciclo.
	uint8_t mode;       //  ON 1, OFF 0, TOGGLE 2.
} my_led;


/*==================[internal functions declaration]=========================*/
void retardo(uint8_t periodo)
{
 sleep(periodo); //	Pausa el programa por un periodo de tiempo dado.
};
void prende(uint8_t n_led)
{
	printf("Enciende el led %d \r\n",n_led);
};

void apaga(uint8_t n_led)
{
	printf("Se apaga el led %d \r\n",n_led);

};

void toogle(uint8_t n_led,uint8_t ciclos,uint8_t periodo)
{

	uint8_t i=0;

	while(i<ciclos)
	{
		prende(n_led);
		retardo(periodo);
		apaga(n_led);
		retardo(periodo);
		i++;
	}

}


void actualizacion(my_led *ledPtr)
{
	switch(ledPtr->mode)
	{
		case(0):
			apaga(ledPtr->n_led);
			break;
		case(1):
			prende(ledPtr->n_led);
			break;
		case(2):
			toogle(ledPtr->n_led,ledPtr->n_ciclos,ledPtr->periodo);
			break;
	}
}

int main(void)
{
	my_led led, *ledPtr;
	ledPtr=&led;

	printf("Introduce un valor para el modo: \r\n");
	printf("Modo 0: apaga led \r\n");
	printf("Modo 1: enciende led \r\n");
	printf("Modo 2: modo toogle \r\n");
	scanf("%" SCNu8, &led.mode);
	printf("Ingrese el número de led: \n");
	scanf("%" SCNu8, &led.n_led);

	if(ledPtr->mode==2)
	{
		printf("Introduzca un nro. de ciclo: \n");
		scanf("%" SCNu8,&led.n_ciclos);

		printf("Introduzca un valor de periodo: \n");
		scanf("%" SCNu8,&led.periodo);
	}

	actualizacion(ledPtr);

	sleep(3);
	return(0);
}

/*==================[end of file]============================================*/

