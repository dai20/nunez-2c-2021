/*
 * DisplayITS_E0803.c
 *
 *  Created on: 10 sep. 2021
 *      Author: Daiana Inés Nuñez
 */
/* All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */


/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup DisplayITS_E0803
 ** @{
**  @file DisplayITS_E0803.h
**
/** @brief This is a driver for the Display ITS E0803. Displays 3 digits up to 999.
 **
 **
 **/
/*Initials     Name
 * ---------------------------
 *	DN		 Daiana Inés Nuñez
 */
/*
 *modification history (new versions first)
 * -----------------------------------------------------------
 * 20210910 v0.1 initials initial version Daiana
 *
 *
 */
#ifndef DisplayITS_E0803_H
#define DisplayITS_E0803_H

/*==================[inclusions]=============================================*/
#include "bool.h"
#include "gpio.h"
#include <stdint.h>

/*==================[macros]=================================================*/
#define lpc4337            1
#define mk60fx512vlq15     2

/*==================[typedef]================================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/
/** @fn bool ITSE0803Init(gpio_t * pins)
 * @brief  Inicializa el display en los pines brindados.
 * @param[in] recibe un puntero a un arreglo de estructuras. La función recibe un arreglo de la
 * siguiente forma gpio_t  pins [7] = {D1,D2,D3,D4,Sel_0,Sel_1,Sel_2}.
 * @return TRUE if no error //Retorna TRUE si no hubo error.
**/
bool ITSE0803Init(gpio_t * pins);


/** @fn bool ITSE0803DisplayValue(uint16_t data)
 * @brief Muestra el dato en el display, el valor que ingresa como parámetro.
 * @param[in] recibe un entero sin signo de 16 bits.
 * @return Retorna TRUE si no hubo error.
 */
bool ITSE0803DisplayValue(uint16_t data);


/** @fn uint16_t ITSE0803ReadValue(void)
 * @brief Guarda en una variable y retorna el valor que muestra el Display.
 * @param[in] No parameter.
 * @return Retona el valor que muestra el display.
 */
uint16_t ITSE0803ReadValue(void);


/** @fn bool ITSE0803Deinit(gpio_t * pins)
 * @brief No hace nada. Libera los pines usados por el display.
 * @param recibe un puntero a un arreglo de estructuras.
 * @return Retorna TRUE si no hubo error.
 */
bool ITSE0803Deinit(gpio_t * pins);

/*==================[external data definition]===============================*/

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
#endif  DisplayITS_E0803_H
