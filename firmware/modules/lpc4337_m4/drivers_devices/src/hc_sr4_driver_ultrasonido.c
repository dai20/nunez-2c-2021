	/* Copyright 2016, 
 * Leandro D. Medus
 * lmedus@bioingenieria.edu.ar
 * Eduardo Filomena
 * efilomena@bioingenieria.edu.ar
 * Juan Manuel Reta
 * jmrera@bioingenieria.edu.ar
 * Sebastian Mateos
 * smateos@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \brief Bare Metal driver for leds in the EDU-CIAA board.
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *	LM			Leandro Medus
 * EF		Eduardo Filomena
 * JMR		Juan Manuel Reta
 * SM		Sebastian Mateos
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20160422 v0.1 initials initial version Leando Medus
 * 20160807 v0.2 modifications and improvements made by Eduardo Filomena
 * 20160808 v0.3 modifications and improvements made by Juan Manuel Reta
 * 20180210 v0.4 modifications and improvements made by Sebastian Mateos
 * 20190820 v1.1 new version made by Sebastian Mateos
 */

/*==================[inclusions]=============================================*/
#include "led.h"
#include "gpio.h"
#include "delay.h"
#include "hc_sr4_driver_ultrasonido.h"
/*==================[macros and definitions]=================================*/

uint32_t acumulador=0;
int16_t valor_distancia;
#define PARAMETRO_CM 58
#define PARAMETRO_PULG 148
gpio_t p_trigger;
gpio_t p_echo;


/*==================[internal data declaration]==============================*/

bool HcSr04Init(gpio_t echo, gpio_t trigger){

	//ASIGNO LOS PARAMETROS A VARIABLES YA DECLARADAS
	p_trigger=trigger;
	p_echo= echo;

	//INICIALIZO LOS GPIO
	GPIOInit(p_echo, GPIO_INPUT);
	GPIOInit(p_trigger, GPIO_OUTPUT);

	//me aseguro que esta en cero
	GPIOOff(p_trigger);

	return (true);

}
int16_t HcSr04ReadDistanceCentimeters(void){

		acumulador =0;

		GPIOOn(p_trigger);
		DelayUs(10);
		GPIOOff(p_trigger);

		while (GPIORead(p_echo) == 0)
		{

		}
		while (GPIORead(p_echo) == 1)
		{
			DelayUs(1);
			acumulador++;

		}

		valor_distancia = acumulador/PARAMETRO_CM;

		return valor_distancia;

}
int16_t HcSr04ReadDistanceInches(void){

		acumulador = 0;

		GPIOOn(p_trigger);
		DelayUs(10);
		GPIOOff(p_trigger);


		while (GPIORead(p_echo) == false)
		{

		}
		while (GPIORead(p_echo) == true)
		{
			acumulador ++;
			DelayUs(1);
		}

		valor_distancia = acumulador/PARAMETRO_PULG;

		return valor_distancia;


}
bool HcSr04Deinit(gpio_t echo, gpio_t trigger){

		GPIODeinit ();
		return true;

}










/*==================[end of file]============================================*/
