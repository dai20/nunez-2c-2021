/*
 * DisplayITS_E0803.c
 *
 *  Created on: 10 sep. 2021
 *      Author: Daiana Inés Nuñez
 *
 * This driver provides functions to generate numeric output interface of LPC4337.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *
 */

/*==================[inclusions]=============================================*/
#include "DisplayITS_E0803.h"
#include "bool.h"
#include "gpio.h"
#include "delay.h"


/*==================[macros and definitions]=================================*/
#define MAX_VALUE 999
#define WRITE_DELAY 1   // 1 ms
uint8_t digits_bcd [3]={0}; //Arreglo Global.
uint16_t display_value=0; //Variable Global.
//uint16_t data;
uint8_t p_lines [4]; // D1, D2, D3, D4, Arreglos de estructuras Globales.
uint8_t sel_lines [3]; // SEL_1, SEL_2, SEL_3



//
/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

bool verificar (uint16_t data) // Verifica si el valor ingresado es correcto.
{
	if (data<=MAX_VALUE){
		return true;
	}
	else{
		return false;
	}
}

void ToBcd (uint16_t data) //Pasa el número de decimal a BCD.
{
	uint8_t i;
	for (i=0;i<3;i++)
	{
		digits_bcd [2-i]= data%10; // Asigno el resto a la dirección donde apunta el puntero.
		data = data/10;
	}
}

void ToDisplay()
{
	uint8_t i, j, mask;
	for (i=0; i<3; i++)
	{
		mask = 1;
		for(j=0; j<4; j++)
		{
			if (digits_bcd[i] & mask)
			{
				GPIOOn(p_lines[j]);
			}
			else
			{
				GPIOOff(p_lines[j]);
			}
			mask = mask << 1;
		}
		GPIOOn(sel_lines[i]);
		DelayMs(WRITE_DELAY);
		GPIOOff(sel_lines[i]);
	}

}




/*==================[external data definition]===============================*/


 /*==================[external functions definition]==========================*/

bool ITSE0803Init(gpio_t *pins) // Inicia el Display.
{
		//uint8_t i;

		// port D1, D2, D3,D4.
		//for(i=0; i<7; i++)
		//{
			//if(i<4)
			//{
		GPIOInit(GPIO_LCD_1,GPIO_OUTPUT);
		p_lines[0]= GPIO_LCD_1;
		GPIOInit(GPIO_LCD_2,GPIO_OUTPUT);
		p_lines[1]= GPIO_LCD_2;
		GPIOInit(GPIO_LCD_3,GPIO_OUTPUT);
		p_lines[2]= GPIO_LCD_3;
		GPIOInit(GPIO_LCD_4,GPIO_OUTPUT);
		p_lines[3]= GPIO_LCD_4;

		//	}

		//	else
		//	{
		GPIOInit(GPIO_1,GPIO_OUTPUT);
		sel_lines[0]=GPIO_1;
		GPIOInit(GPIO_3,GPIO_OUTPUT);
		sel_lines[1]=GPIO_3;
		GPIOInit(GPIO_5,GPIO_OUTPUT);
		sel_lines[2]=GPIO_5;

			//}
//}
		return true;
}


bool ITSE0803DisplayValue(uint16_t valor) // Asigna y muestra los valores.
{
		ToBcd(valor);
		ToDisplay();
		display_value = valor;
		return true;
}


uint16_t ITSE0803ReadValue(void)  // Guarda en una variable el valor que muestra el Display.
{
		return display_value;
}


bool ITSE0803Deinit(gpio_t * pins) //No hace nada.
{
	GPIODeinit();
	return true;
}




/*==================[end of file]============================================*/
