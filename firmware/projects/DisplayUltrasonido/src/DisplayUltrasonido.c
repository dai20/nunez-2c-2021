/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * Autor: Daiana Inés Nuñez
 *
 *
 *
 *
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/DisplayUltrasonido.h"       /* <= own header */
#include "systemclock.h"				/* para Proyecto 2 */
#include "led.h"
#include "switch.h"
#include "gpio.h"
#include "delay.h"
#include "bool.h"
#include "hc_sr4_driver_ultrasonido.h"	/* Driver Ultrasonido*/
#include "DisplayITS_E0803.h"			/* Driver Display*/



/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/
int16_t valor_distancia;
uint8_t tecla, estado;
bool activar=false;
bool hold=false;
bool aux;
gpio_t * pins;

/*==================[internal functions declaration]=========================*/
//1)Encender el/los Led/s correspondientes:
void CompararEncerder(int16_t valor_distancia)
{
	if (valor_distancia>0 && valor_distancia<2 ){
			LedOff(LED_RGB_B);
			LedOff(LED_1);
			LedOff(LED_2);
			LedOff(LED_3);
	}

	else if(valor_distancia>2 && valor_distancia<10)
	{
			LedOn(LED_RGB_B);
			LedOff(LED_1);
			LedOff(LED_2);
			LedOff(LED_3);
	}
	else if(valor_distancia>10 && valor_distancia<20)
	{
			LedOn(LED_RGB_B);
			LedOn(LED_1);
			LedOff(LED_2);
			LedOff(LED_3);
	}
	else if(valor_distancia>20 && valor_distancia<30)
	{
			LedOn(LED_RGB_B);
			LedOn(LED_1);
			LedOn(LED_2);
			LedOff(LED_3);
	}
	else if(valor_distancia>30)
	{
			LedOn(LED_RGB_B);
			LedOn(LED_1);
			LedOn(LED_2);
			LedOn(LED_3);
	}

}


/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/



int main(void)
{
	//gpio_t * pins [7]= {GPIO_LCD_1, GPIO_LCD_2,GPIO_LCD_3,GPIO_LCD_4,GPIO_1,GPIO_3,GPIO_5};

	// Inicializar.
	SystemClockInit();
	LedsInit();
	aux=ITSE0803Init(&pins);
	aux=HcSr04Init(GPIO_T_FIL2, GPIO_T_FIL3);
	SwitchesInit();

    while(1)
    {
    	tecla= SwitchesRead();
    	if(tecla==SWITCH_1)//3)Usar TEC1 para activar y detener la medición.
    	{
    		activar=!activar;
    	}
    	else if (tecla==SWITCH_2)	//4)Usar TEC2 para mantener el resultado (“HOLD”).
    	{
    		hold=!hold;
    	}

    	if (hold==1){
    		CompararEncerder(valor_distancia);
    		aux=ITSE0803DisplayValue(valor_distancia);
    	}
    	else{
    		if (activar==1){
    	    	// leer distancia ultrasonido.
    	    	valor_distancia= HcSr04ReadDistanceCentimeters();
    	    	CompararEncerder(valor_distancia);
    	    	aux=ITSE0803DisplayValue(valor_distancia);
    		}
    	    else{
    	    	valor_distancia=0;
    	    	aux=ITSE0803DisplayValue(valor_distancia);
    	    	CompararEncerder(valor_distancia);
    	    }
    	}
    	DelaySec(1);

    }

	aux=ITSE0803Deinit(&pins);
    return 0;
}

/*==================[end of file]============================================*/

