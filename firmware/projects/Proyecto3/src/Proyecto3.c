/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * Autor: Daiana Inés Nuñez
 *
 *
 *
 *
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Proyecto3.h"       /* <= own header */
#include "systemclock.h" 			/* para Proyecto 2 */
#include "led.h"
#include "switch.h"
#include "gpio.h"
#include "delay.h"
#include "hc_sr4_driver_ultrasonido.h" /* Driver Ultrasonido*/
#include "DisplayITS_E0803.h"		   /* Driver Display*/
#include "bool.h"
#include "timer.h"					/* para Proyecto 3 */
#include "uart.h"


/*==================[macros and definitions]=================================*/
int16_t valor_distancia;
uint8_t dato_uart;
uint8_t teclas, estado;
bool activar=false;
bool hold=false;
bool aux;
gpio_t * pins[7];
/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/
//Interrupciones por teclas de la placa:
/*
 @fn void DoTecla1(){activar=!activar;}
 @brief Con tecla1 Activa/desactiva la lectura.
  */
void DoTecla1(){activar=!activar;}

/*
 @fn void DoTecla2(){hold=!hold;}
 @brief Con tecla2 detiene la lectura.
  */
void DoTecla2(){hold=!hold;}

//Enciende el/los Led/s correspondientes según la distancia:
void CompararEncerder(int16_t valor_distancia)
{
	if (valor_distancia>0  && valor_distancia<2){
		LedOff(LED_RGB_B);
		LedOff(LED_1);
		LedOff(LED_2);
		LedOff(LED_3);
	}

	else if(valor_distancia>2 && valor_distancia<10)
	{
		LedOn(LED_RGB_B);
		LedOff(LED_1);
		LedOff(LED_2);
		LedOff(LED_3);
	}

	else if(valor_distancia>10 && valor_distancia<20)
	{
		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOff(LED_2);
		LedOff(LED_3);
	}
	else if(valor_distancia>20 && valor_distancia<30)
	{
		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOn(LED_2);
		LedOff(LED_3);
	}
	else if(valor_distancia>30)
	{
	LedOn(LED_RGB_B);
	LedOn(LED_1);
	LedOn(LED_2);
	LedOn(LED_3);
	}

}

//Interrupción por Timer:
void DoTimer(){
	if (hold==1){
		CompararEncerder(valor_distancia);
		aux=ITSE0803DisplayValue(valor_distancia);
	}
	else{
		if (activar==1){
			// leer distancia ultrasonido.
			valor_distancia= HcSr04ReadDistanceCentimeters();
	    	CompararEncerder(valor_distancia);
	    	aux=ITSE0803DisplayValue(valor_distancia);
	    	UartSendString(SERIAL_PORT_PC,UartItoa(valor_distancia, 10));//UartItoa transforma el valor numérico de 32bits a una cadena de caracteres.
	    	UartSendString(SERIAL_PORT_PC," cm\r\n");
	    }
		else{
			valor_distancia=0;
			aux=ITSE0803DisplayValue(valor_distancia);
			CompararEncerder(valor_distancia);
		}
	}

}

//Interrupción por Uart:
void DoUart()
{
	UartReadByte(SERIAL_PORT_PC, &dato_uart);//lee desde el puerto la info de 1 byte.
	switch (dato_uart){
		case 'a':
		{
			activar=!activar;
			break;
		}
		case 'h':
		{
			hold=!hold;
			break;
		}
		default:
		{
			UartSendString(SERIAL_PORT_PC,"error: a=activar/desactivar, h= hold\n");
		}
}


}
/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	// Inicializar Clock, leds, switches, pines del Display y sensor Ultrasonido.
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	aux=ITSE0803Init(&pins);
	aux=HcSr04Init(GPIO_T_FIL2, GPIO_T_FIL3);


	//Activa las Interrupciones por switch:
	SwitchActivInt(SWITCH_1, &DoTecla1);
	SwitchActivInt(SWITCH_2, &DoTecla2);

	//Activa las Interrupciones por uart:
	serial_config use_uart = {SERIAL_PORT_PC,115200,&DoUart};
	UartInit(&use_uart);

	//Activa las Interrupciones por timer:
	timer_config use_timer = {TIMER_A,1000,&DoTimer};
	TimerInit(&use_timer);
	TimerStart(TIMER_A);
	UartSendString(SERIAL_PORT_PC,"CONFIGURACIÓN EXITOSA");


	while(1)
	{

	}

	return 0;
}

/*==================[end of file]============================================*/

