/* Copyright 2018,
 * Juan Manuel Reta
 * jmreta@ingenieria.uner.edu.ar
 * Facultad de Ingenieria
 * Universidad Nacional de Entre Rios
 * Argentina
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
 
/*
 * Initials     Name
 * ---------------------------
 *	DN			Daiana Inés Nuñez
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20210917 v0.0.1 initials initial version
 */

#ifndef _PROYECTO3_H
#define _PROYECTO3_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif


/*==================[internal functions declaration]=========================*/
/*Interrupciones por teclas de la placa:
@fn void DoTecla1(){activar=!activar;}
@brief Al presionar la tecla 1 Activa/desactiva la lectura.
@param[in] No parameter.
*/
void DoTecla1(){activar=!activar;}

/*
 @fn void DoTecla2(){hold=!hold;}
 @brief Al presionar la tecla 2 detiene la lectura.
 @param[in] No parameter.
 */
void DoTecla2(){hold=!hold;}

/*
@fn void CompararEncerder(int16_t valor_distancia)
@brief Compara el valor de lectura con las distacias de referencia y enciende los leds
correspondientes.
@param[in] recibe el valor de distancia que lee el sensor ultrasonio.
 */
void CompararEncerder(int16_t valor_distancia)

/*
 @fn void DoTimer()
 @brief Interrupción por timer.Actua cuando se desborda el timer a los 1000ms.
 @param[in] No parameter.
 */
void DoTimer()


/*
 @fn void DoUart()
 @brief Interrupción por Uart:Cuando se presiona la tecla ¨a¨ se Activa/desactiva la lectura.
 *Cuando se presiona la tecla ¨h¨ se detiene la lectura. En caso de presionar otra tecla se manda mensaje
 *de error.
 @param[in] No parameter.
 */
void DoUart()


/*==================[end of file]============================================*/



#endif /* #ifndef _BLINKING_H */
